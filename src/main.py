import GameOfLife as gol 
import numpy as np 

if __name__ == "__main__":
    
    grid = [[0, 0, 0, 1, 0, 0],
            [0, 0, 0, 1, 1, 0],
            [1, 1, 0, 0, 1, 1],
            [0, 1, 1, 0, 0, 1],
            [0, 0, 1, 1, 0, 0],
            [0, 1, 0, 1, 1, 0]]
    grid = np.array(grid, dtype='int')

    time_unit = 2

    game_of_life = gol.GameOfLife(grid, time_unit)

    game_of_life.run(n_gen=60)